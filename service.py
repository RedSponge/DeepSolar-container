import sys
from flask import Flask
from flask import request
import pv_detector
from base64 import b64encode, b64decode

app = Flask(__name__)

@app.route("/process")
def process_image():
    img_data = request.json["img"]
    data = b64decode(img_data)
    with open("hello.tiff", "wb") as f:
        f.write(data)
    pv_detector.process_image("hello.tiff")
    with open("output.tiff", "rb") as f:
        output = f.read()
    
    output = b64encode(output)
    return {"result": output}

port = int(sys.argv[1] if len(sys.argv) > 1 else 5000)
print(port)
app.run(host='0.0.0.0', port=port)