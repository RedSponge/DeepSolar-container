import sys
from subprocess import Popen
import os
import tifffile
from PIL import Image
import shutil
from pathlib import Path
import tiff_helper

def crop_image(path):
    im = Image.open(path)
    
    width, height = im.size
    num_slices = max(1, min(width, height) / 150)

    slice_width, slice_height = width / num_slices, height / num_slices
    print "Image size is ({}, {}). There will be {}^2 slices. Each slice's size will be ({}, {})".format(width, height, num_slices, slice_width, slice_height)

    cropped_images = []
    for x in range(num_slices):
        for y in range(num_slices):
            slc = (x * slice_width, y * slice_height, min((x + 1) * slice_width, width), min((y + 1) * slice_height, height))
            print "Cropping {}".format(slc)
            cropped = im.crop(slc)
            cropped_images.append(cropped)

    
    im.close()
    return cropped_images, num_slices

def write_to_spi_dir(images):
    data_dir = Path(__file__).absolute().parent / 'DeepSolar' / 'SPI_eval'

    shutil.rmtree(str(data_dir), ignore_errors=True)
    data_dir.mkdir(parents=True)
    
    imgs_dir = data_dir / '1' / '1'
    imgs_dir.mkdir(parents=True)

    for i, img in enumerate(images):
        img.save(str(imgs_dir / '{}.png'.format(i + 1)))
    meta_file = data_dir / 'eval_set_meta.csv'
    with open(str(meta_file), 'w') as f:
        f.write('a,b,c,d,e,region\n')
        f.write('0,0,0,0,0,r\n' * len(images))

def run_deepsolar(python_exec):
    os.system('cd DeepSolar; {} ./generate_data_list.py'.format(python_exec))
    os.system('cd DeepSolar; {} ./test_segmentation.py'.format(python_exec))
    

def stitch_cams(image_files, num_slices):
    imgs = [Image.open(f) for f in image_files]
    height = sum(i.size[1] for i in imgs[:num_slices])
    width = sum(i.size[0] for i in imgs[::num_slices])
    print "Stitching image to size ({}, {})".format(width, height)
    out = Image.new(mode='RGB', size=(width, height))
    for x in range(num_slices):
        for y in range(num_slices):
            out.paste(imgs[x * num_slices + y], (x * 100, y * 100))
    return out


def rescale_cam(image_path, cam):
    tiff_raster = Image.open(image_path)
    cam = cam.resize(tiff_raster.size)
    return cam


def process_image(image_path, python_exec='python2.7'):
    cropped, num_slices = crop_image(image_path)
    write_to_spi_dir(cropped)
    run_deepsolar(python_exec)
    image_files = ["DeepSolar/segmentation_results/TP/1_{}_CAM.png".format(i + 1) for i in range(num_slices * num_slices)]
    big_cam = stitch_cams(image_files, num_slices)
    big_cam = rescale_cam(image_path, big_cam)

    big_cam.save("rescaled.png")

    tiff_helper.copy_pixels_from_image_to_tiff(image_path, "rescaled.png", "output.tiff")
    
    

    
        

def main(argv):
    if len(argv) != 2:
        print "Usage: {} <Image>".format(argv[0])
        return -1
    
    image_path = argv[1]

    process_image(image_path)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))