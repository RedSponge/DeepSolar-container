import requests
from base64 import b64encode, b64decode
import sys

def process_image(server, src_file, dst_file):
    """
        Send an image to the deepsolar service, and save the result.
        :param src_file: The source file to send to the service
        :param dst_file: The destination file to save the result to.
    """
    with open(src_file, "rb") as f:
        data = f.read()
    data = b64encode(data)
    result = requests.get("{}/process".format(server), json={"img": data.decode()})
    data = result.json()["result"]
    data = b64decode(data)
    with open(dst_file, "wb") as f:
        f.write(data)
