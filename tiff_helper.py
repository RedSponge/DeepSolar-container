from osgeo import gdal

def copy_pixels_from_image_to_tiff(src_tiff_path, image_path, new_tiff_path):
    #1.    
    tiff_file = gdal.Open(src_tiff_path)
    #2.
    geotransform = tiff_file.GetGeoTransform()
    projection = tiff_file.GetProjection()
    band = tiff_file.GetRasterBand(1)    
    xsize = band.XSize
    ysize = band.YSize
    #3.
    array = band.ReadAsArray()
    tiff_file = None #close it
    band = None #close it
    #4.
    image_file = gdal.Open(image_path)
    band = image_file.GetRasterBand(1)    
    image_array = band.ReadAsArray()
    array = image_array
    #5.
    driver = gdal.GetDriverByName('GTiff')
    new_tiff = driver.Create(new_tiff_path,xsize,ysize,1,gdal.GDT_Int16)
    new_tiff.SetGeoTransform(geotransform)
    new_tiff.SetProjection(projection)
    new_tiff.GetRasterBand(1).WriteArray(array)
    new_tiff.FlushCache() #Saves to disk 
    new_tiff = None #closes the file