FROM ubuntu:20.04

# Install general programs
RUN apt update -y
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN apt install -y python2.7 gcc python2.7-dev g++ software-properties-common


# Install GDAL
RUN add-apt-repository -y ppa:ubuntugis/ppa
RUN apt update -y
RUN apt install -y gdal-bin libgdal-dev


ADD . /root/pv


# Untar checkpoints
RUN mv /root/pv/ckpt /root/pv/DeepSolar/ckpt
WORKDIR /root/pv/DeepSolar/ckpt
RUN ls -la .
RUN tar -xvf /root/pv/DeepSolar/ckpt/inception-v3-2016-03-01.tar.gz
RUN tar -xvf /root/pv/DeepSolar/ckpt/inception_classification.tar.gz
RUN tar -xvf /root/pv/DeepSolar/ckpt/inception_segmentation.tar.gz


WORKDIR /root/pv

RUN python2.7 /root/pv/scripts/get-pip.py

RUN pip install -r /root/pv/DeepSolar/requirements.txt
RUN pip install flask tifffile pillow gdal


ENTRYPOINT ["python2.7", "/root/pv/service.py"]
